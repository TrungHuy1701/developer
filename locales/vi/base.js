export default {
    // menu 
    menu_detect: "Phát hiện",
    menu_identification: "Nhận dạng",
    menu_data: "Cơ sở dữ liệu",
    menu_people: "Người",
    menu_vehicle: "Phương tiện",
    menu_behavior: "Hành vi",
    menu_add: "Thêm",
    menu_delete: "Xoá",
    menu_cancel: "Huỷ bỏ",
    menu_camera_map_title: "Bản đồ camera",
    menu_circle_camera: "Camera tròn",
    menu_long_body_camera: "Camera thân dài",
    menu_wifi_camera: "Camera wifi",
    menu_dome_camera: "Camera dome",
    menu_all: "Tất cả",

    // header 
    header_from: "Từ",
    header_to: "Đến",
    header_search_placeholder: "Tìm người",
    header_show: "Hiển thị",
    header_save: 'Lưu',
    header_cancel: 'Huỷ bỏ',
    header_close: 'Đóng',
    header_edit: 'Chỉnh sửa',
    header_add: 'Thêm',
    header_delete: 'Xoá',
    header_delete_all: 'Xoá hết',
    header_back: 'Trở lại',

    //form
    invalid_email: "E-mail không hợp lệ, vui lòng thử lại!",
    input_null: "Vui lòng nhập vào đây!",
    input_error: "Vui lòng không nhập ký tự đặt biệt!",
    identity_length: "Chứng minh nhân dân từ 9 số trở lên",
    phone_length: "Số điện thoại 10 số",
    phone_error: "Số điện thoại không hợp lệ",
    input_pass_null: "Vui lòng nhập mật khẩu!",
    input_pass_min: "Mật khẩu phải chứa ít nhất 8 kí tự",
    input_pass_max: "Mật khẩu không quá 25 kí tự",
    input_file_max: "Tập tin được không vượt quá 2.5mb",
    input_file_invalid: "Tập tin không hợp lệ",

    // status 
    waring: 'Có lỗi',
    success: 'Thành công',
    error: 'Lỗi',
    update_success: 'Cập nhật thành công',
    create_success: "Tạo mới thành công",
    success_add: "Thêm thành công",
    success_delete: "Xóa thành công",

    langague_vi: 'VN',
    langague_en: 'EN',
    view_all: "Xem tất cả",
    text_no_data: "Không có dữ liệu",
    text_no_data_system: "Không có dữ liệu trong hệ thống",
    birth_day_error: 'Ngày sinh không hợp lệ',
    error_server_message: 'Đã xảy ra lỗi',
    unknown: "Không xác định",
    header_control_center: "TRUNG TÂM ĐIỀU KHIỂN",

    //constant helper
    quality_low: "Thấp",
    quality_medium: "TB",
    quality_high: "Cao",

    //Pagination
    pagination_label_page: 'Trang',
    pagination_of: 'của',
    pagination_in_total: "trong tổng số",
    pagination_records: "mục",
    pagination_first: "Đầu",
    pagination_last: "Cuối",
    pagination_next: "Sau",
    pagination_prev: "Trước",
    pagination_search_page: "Tìm trang",
    pagination_page_id: "Mã",


    // notification 
    notification_error: 'Không kết nối được Thông Báo!',
    notification_success: 'Kết nối thành công!',
    notification_delete_success: 'Xóa vùng cảnh báo thành công',
    notification_delete_fail: 'Xóa vùng cảnh báo thất bại',
    notification_create_success: 'Tạo vùng cảnh báo thành công',
    notification_create_fail: 'Tạo vùng cảnh báo thất bại'
}