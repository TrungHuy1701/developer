export default {
    // menu 
    menu_detect: "Detect",
    menu_identification: "Identification",
    menu_data: "Database",
    menu_people: "People",
    menu_vehicle: "Vehicles",
    menu_behavior: "Behaviors",
    menu_add: "Add",
    menu_delete: "Delete",
    menu_cancel: "Cancel",
    menu_camera_map_title: "Camera map",
    menu_circle_camera: "Circle camera",
    menu_long_body_camera: "Long body camera",
    menu_wifi_camera: "Wifi camera",
    menu_dome_camera: "Dome camera",
    menu_all: "All",

    // header 
    header_from: "From",
    header_to: "To",
    header_search_placeholder: "Person Search",
    header_show: "Show",
    header_save: 'Save',
    header_cancel: 'Cancel',
    header_close: 'Close',
    header_edit: 'Edit',
    header_add: 'Add',
    header_delete: 'Delete',
    header_delete_all: 'Delete all',
    header_back: 'Back',

    //form
    invalid_email: "Invalid e-mail, please try again!",
    input_null: "Please enter in here!",
    input_error: "Please not enter the special character!",
    identity_length: "Identity Card from 9 numbers or more",
    phone_length: "Phone numbers from 10 numbers",
    phone_error: "Invalid phone number",
    input_pass_null: "Please input your Password!",
    input_pass_min: "New password must have at least 8 letters",
    input_pass_max: "New password cannot exceed 25 letters",
    input_file_max: "File should not exceed 2.5mb",
    input_file_invalid: "File is not valid",

    // status 
    warning: 'An error occurred',
    success: 'Success',
    error: 'Error',
    update_success: 'Update success',
    create_success: 'Create success',
    success_add: "Add success",
    success_delete: "Delete success",

    langague_vi: 'VN',
    langague_en: 'EN',
    view_all: "View all",
    text_no_data: "No data",
    text_no_data_system: "No data in the system",
    birth_day_error: 'Invalid date of birth',
    error_server_message: 'An error has occurred',
    unknown: "Unknown",
    header_control_center: "CONTROL CENTER",

    //constant helper
    quality_low: "L",
    quality_medium: "M",
    quality_high: "H",

    //pagination
    pagination_label_page: 'Page',
    pagination_of: 'of',
    pagination_in_total: "in total",
    pagination_records: "records",
    pagination_first: "First",
    pagination_last: "Last",
    pagination_next: "Next",
    pagination_prev: "Prev",
    pagination_search_page: "Search page",
    pagination_page_id: "Page",

    // notification 
    notification_error: 'Notification disconnected!',
    notification_success: 'Notification connected!',
    notification_delete_success: 'Delete area success',
    notification_delete_fail: 'Delete area fail',
    notification_create_success: 'Create area success',
    notification_create_fail: 'Create area fail'
}