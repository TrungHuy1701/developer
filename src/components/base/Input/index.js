import InputCheckboxColor from './InputCheckboxColor'
import InputIcon from './InputIcon'
import InputPassword from './InputPassword'
import InputSearch from './InputSearch'

export {
    InputCheckboxColor, InputIcon, InputPassword, InputSearch
}