import React, { useState } from 'react';
import { Input, Form } from 'antd';
import { useTranslation } from 'react-i18next';

const InputPassword = (props) => {
    const { className, dependencies, rules, name, placeholder, onChange, onBlur, form } = props
    const [isShow, setShowText] = useState()
    const { t } = useTranslation()
    return (
        <Form.Item className={className} name={name} rules={rules} dependencies={dependencies}>
            <div className="input_block">
                <i className="login__icon fas fa-lock color_f4a239"></i>
                <Input
                    placeholder={placeholder && t(placeholder)}
                    className="form-control input_password"
                    type={isShow ? "text" : "password"}
                    onChange={onChange}
                    onBlur={onBlur}
                    name={name}
                    value={form && form.getFieldValue(name)}
                />
                <i
                    className={"login__icon toggle_password far text_8b8b8b fa-eye" + (isShow ? "" : "-slash")}
                    onClick={() => setShowText(!isShow)}
                />
            </div>
        </Form.Item>
    )
}
export default InputPassword;