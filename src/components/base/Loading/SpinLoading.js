import React from 'react';
import { Spin } from "antd"

const SpinLoading = ({ className, spinning, style, children }) => {
    return (
        <Spin className={className ? className : "loading_full"} spinning={spinning ? true : false} style={style}>
            {children}
        </Spin>
    )
}
export default SpinLoading;