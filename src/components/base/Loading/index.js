import SkeletonLoading from './SkeletonLoading'
import SpinLoading from './SpinLoading'
import StaticLoading from './StaticLoading'

export {
    SkeletonLoading, SpinLoading, StaticLoading
}