import React from 'react';
import { Modal } from 'antd';
import { useTranslation } from 'react-i18next';

const ModalReload = ({ visible, setVisible, className = "modal_reload" }) => {

    const { t } = useTranslation()

    const onClose = () => {
        setVisible(false)
    }

    return (
        <Modal
            visible={visible}
            mask={true}
            maskClosable={false}
            keyboard={false}
            footer={null}
            width={360}
            onCancel={onClose}
            className={className}
            zIndex={100000000}
        >
            <div className="modal_reload__container">
                <button className="close" onClick={onClose}>
                    <i class="las la-times"></i>
                </button>
                <div className="modal_reload__text">{t('login_warning')}</div>
                <div className="modal_reload__button">
                    <button className="btn btn_orange button_function__upload" onClick={() => window.location.reload()}>
                        <i className="las la-redo-alt mr-2"></i>
                        Reload
                    </button>
                </div>
            </div>
        </Modal>
    )
}
export default ModalReload;