import React, { useState } from 'react';
import { Modal } from 'antd';
import { translate } from '../../../functions/Utils';


const ModalCustom = (props) => {
    const { visible, widthModal, setVisible, title, classBody, classModal, footer } = props
    const [isClearData, setClearData] = useState(false)

    const afterClearData = () => {
        setClearData(false)
    }
    return (
        <Modal
            footer={footer}
            forceRender
            visible={visible}
            onCancel={() => setVisible(false)}
            className={classModal ? classModal : ''}
            width={widthModal ? widthModal : '390px'}
        >
            <div className="modal-content modal_special">
                <div className="modal-header">
                    <h5 className="modal-title">{title ? translate(title) : translate("example_banner_title")}</h5>
                    <button type="button" className="close" onClick={() => setVisible(false)} data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i className="las la-times"></i></span>
                    </button>
                </div>
                <div className={`modal-body ${classBody ? classBody : ''}`}>
                    {props.children}
                </div>
            </div>
        </Modal>
    )
}

export default ModalCustom;
