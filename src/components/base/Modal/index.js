import ModalCustom from './ModalCustom'
import ModalDelete from './ModalDelete'
import ModalReload from './ModalReload'

export {
    ModalCustom, ModalDelete, ModalReload
}