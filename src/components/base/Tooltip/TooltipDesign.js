import { Tooltip } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

const TooltipDesign = ({ title = "", destroyTooltipOnHide = true, placement = "bottom", children }) => {
    const { t } = useTranslation()
    return (
        <Tooltip
            title={t(title)}
            destroyTooltipOnHide={destroyTooltipOnHide}
            placement={placement}
        >
            {children}
        </Tooltip>
    )
}

export default TooltipDesign