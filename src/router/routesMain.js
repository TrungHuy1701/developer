import React, { lazy } from 'react';

const Elements = lazy(() => import('../templates/Elements/Elements'));
const PageErrorGlobal = lazy(() => import('../templates/ErrorPage/PageErrorGlobal'));
const Page404 = lazy(() => import('../templates/ErrorPage/Page404'));
const PageLogin = lazy(() => import('../templates/Authen/Login/PageLogin'));

// page url 
const PUBLIC = [
    {
        "path": '/',
        "component": PageLogin
    },
]
const PRIVATE = [
    {
        "path": '/elements',
        "component": Elements
    },
]
const ERROR = [
    {
        "path": '/server-error',
        "component": PageErrorGlobal
    },
    {
        "path": "/404",
        "component": Page404,
    }
]



export { PRIVATE, PUBLIC, ERROR }