import { MODE_ENV } from '../../env/env'

let DOMAIN = {
    api: ''
};
if (process.env.TARGET_ENV === 'prod') {
    DOMAIN = MODE_ENV.prod;
    DOMAIN.api = MODE_ENV.prod.api
} else if (process.env.TARGET_ENV === 'dev') {
    DOMAIN = MODE_ENV.dev;
    DOMAIN.api = MODE_ENV.dev.api
} else if (process.env.TARGET_ENV === 'sta') {
    DOMAIN = MODE_ENV.sta;
    DOMAIN.api = MODE_ENV.sta.api
}
// else if (process.env.TARGET_ENV === 'domain') {
//     DOMAIN.api = process.env.DOMAIN_URL
// }
else {
    DOMAIN = MODE_ENV.local;
    DOMAIN.api = ""
    DOMAIN.cloudapi = ""
}

let PREFIX = 'api/v2/';

export default {
    //example
    EXAMPLE: DOMAIN.api + PREFIX + "pokemon/ditto",
}