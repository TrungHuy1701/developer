import api from 'services/api';
import { getHeader, getUrl, handleRequest } from 'functions/ServiceHandle';

export const commonService = {
    getExample({ params }) {
        const requestOptions = {
            method: "GET",
            headers: getHeader(),
        };
        const url = getUrl(api.EXAMPLE, params);
        return handleRequest(url, requestOptions);
    },

}

