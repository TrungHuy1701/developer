import React, { useEffect, useState, memo } from 'react';
import { useDispatch } from "react-redux";
import { commonAction } from '../store/action'
import { getLocalStore, showNotification, translate } from '../functions/Utils'

var Stomp = require('stompjs')

const StompConnectNotification = props => {
    const dispatch = useDispatch();
    const user = getLocalStore('user')

    const [rabbitInfo, setRabbitInfo] = useState();
    const [isConnected, setConnected] = useState(false)
    const [isInit, setInit] = useState(false)
    const [client, setClient] = useState(null);

    useEffect(() => {
        if (user && user.rabbit_notification) {
            if (!isConnected) {
                let response = user.rabbit_notification;
                setRabbitInfo({
                    'RABBIT_USER': response.rabbit_user && response.rabbit_user,// : 'admin',
                    'RABBIT_PASS': response.rabbit_pass && response.rabbit_pass,// : 'admin@123',
                    'RABBIT_HOST': response.rabbit_host && response.rabbit_host,// : 'cctv-mq.minerva.vn',
                    'RABBIT_PORT': response.rabbit_port_wss && response.rabbit_port_wss,// : 9202,
                    'RABBIT_VHOT': response.rabbit_vhot && response.rabbit_vhot,// : 'notification',
                    'RABBIT_EXCHANGE_TOPIC': 'amq.topic',
                })
            }
        } else if (client) {
            client.disconnect()
        }
    }, [])


    // start connect 
    useEffect(() => {
        // Start application
        if (rabbitInfo) {

            /*  
                Scheme: Một scheme cần phải có “ws” or “wss”. “ws” scheme là một kết nối không an toàn về bảo mật, trong khi “wss” thì ngược lại. Các trang chạy qua giao thức HTTP nên sử dụng "ws" WebSockets. Với những trang chạy qua giao thức HTTPS nên sử dụng “wss”. Khi bạn không sử dụng "ws" hoặc "wss" sẽ xảy ra lỗi về kết nối.
            
                + https -> wss
                + http -> ws 
                
            */

            let url = "ws://" + rabbitInfo.RABBIT_HOST + ":" + rabbitInfo.RABBIT_PORT + "/ws"
            let client = Stomp.client(url); // asign the created client as global for sending or subscribing messages
            client.reconnect_delay = 5000;
            client.debug = null;
            // username, password, connectCallback, errorCallback, host
            const onConnectCallback = () => {
                setConnected(true)
            }
            let headers = {
                login: rabbitInfo.RABBIT_USER,
                passcode: rabbitInfo.RABBIT_PASS,
                host: rabbitInfo.RABBIT_VHOT,
            }
            client.connect(headers, onConnectCallback, onErrorCallback);
            setClient(client)
        }
    }, [rabbitInfo])


    // nếu connect đc thì trả response cho UI 
    useEffect(() => {
        if (client && isConnected && !isInit) {
            client.subscribe(`/exchange/${rabbitInfo.RABBIT_EXCHANGE_TOPIC}/` + user.user_id, function (response) { //user.user_id
                printReceivedMessage(response);
            });
            setInit(true)
            // dispatch(chatAction.sendConnectedStatus({ isConnected: true }))
        }
    }, [client, isConnected])


    // on error 
    const onErrorCallback = () => {
        setConnected(false)
        if (user) {
            showNotification({
                type: 'error',
                message: translate('notification_error'),
                title: 'Error'
            })
        }
    }

    // action to store => show view
    const printReceivedMessage = (response) => {
        console.log('rabbit noti:', JSON.parse(response.body));
        dispatch(commonAction.getNotificationItem([JSON.parse(response.body)]))
    }

    return (<div id="rabbit_connect_notification"></div>)
}
export default StompConnectNotification
