import React, { useLayoutEffect, useState, useEffect, Suspense } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, Route, Switch, useLocation, useRouteMatch } from 'react-router-dom';
import { PRIVATE } from 'router/routesMain';

import { Animated } from 'react-animated-css';
import { PAGES_URL } from 'router/routesUrl';
import Page404 from 'templates/ErrorPage/Page404';



const PrivatePage = (props) => {
    const location = useLocation();

    useEffect(() => {
        if (location.pathname != "/") {
            localStorage.setItem('paths', JSON.stringify(["/", location.pathname]))
        }
        else {
            localStorage.setItem('paths', JSON.stringify(["/"]))
        }
        window.addEventListener("beforeunload", e => sessionStorage.clear())
    }, [])


    return (
        <main className={`main_wapper auth_page`}>
            <Suspense fallback={''}>
                <Switch >
                    {PRIVATE.map((data, idx) => (
                        <Route exact key={idx} path={data.path}>
                            <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>
                                <data.component />
                            </Animated>
                        </Route>
                    ))}
                    <Route component={Page404} />
                </Switch>
            </Suspense>
            <div className={`sidebar__sticky-arrow`}>
                <i class={`icon fas fa-caret`}></i>
            </div>
        </main>

    );
}

export default (PrivatePage);