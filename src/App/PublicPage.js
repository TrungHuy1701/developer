import { withTranslation } from 'react-i18next';
import React, { useState, useEffect, useRef, useLayoutEffect, Suspense } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { PUBLIC } from 'router/routesMain';
import { StaticLoading } from 'components/base/Loading';

const PublicPage = (props) => {
    return (
        <main className={`main_wapper public_page`}>
            <Suspense fallback={<StaticLoading />}>
                <Switch >
                    {PUBLIC.map((data, idx) => (
                        <Route exact key={idx} path={data.path}>
                            <data.component />
                        </Route>
                    ))}
                </Switch>
            </Suspense>
        </main>
    );
}

export default withTranslation()(PublicPage);