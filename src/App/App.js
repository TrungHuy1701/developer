//libs
import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Redirect, Route } from "react-router-dom";
import { Provider } from 'react-redux';
import configureStore from '../store';
import { TOKEN, getPathList } from '../functions/Utils'
import { PUBLIC, PRIVATE } from 'router/routesMain';
import PrivatePage from './PrivatePage';
import PublicPage from './PublicPage';
import i18n from 'i18n';
import { I18nextProvider, withTranslation } from 'react-i18next';


const PublicRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        <PublicPage />
    )} />
)
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        <PrivatePage />
    )} />
)
const App = () => {
    const store = configureStore();
    return (
        <Provider store={store}>
            {/* <I18nextProvider i18n={i18n}> */}
            <Router>
                <Switch >
                    <Route exact path={getPathList(PUBLIC)} >
                        <Route render={props => <PublicRoute {...props} />} />
                    </Route>
                    <Route exact path={getPathList(PRIVATE)} >
                        <Route render={props => <PrivateRoute {...props} />} />
                    </Route>
                    <Route render={props => <PrivateRoute {...props} />} />
                </Switch>
            </Router>
            {/* </I18nextProvider> */}
        </Provider>
    );
}

export default withTranslation()(App);
