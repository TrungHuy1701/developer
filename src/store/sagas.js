// export default allSaga;
import { all } from "redux-saga/effects";
import commonSaga from "./common/saga";


function* allSaga() {
    yield all([
        commonSaga(),
    ]);
}

export default allSaga;