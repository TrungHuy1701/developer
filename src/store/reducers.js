import commonReducer from "./common/reducer";

import { combineReducers } from 'redux';

const allReducers = combineReducers({
    commonReducer
});

export default allReducers;