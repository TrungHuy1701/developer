import { put, takeLatest, all, fork, select } from "redux-saga/effects";
import { commonAction } from '../action';
import { commonService } from "../../services/index";

export function* getExample(payload) {
    try {
        const response = yield commonService.getExample({ params: payload.params })
        response.success
            ? yield put({ type: commonAction.EXAMPLE_SUCCESS, response })
            : yield put({ type: commonAction.EXAMPLE_FAILURE, response });

    } catch (err) {
        yield put({ type: commonAction.EXAMPLE_FAILURE, err: { err } });
    }
}

export default function* rootSaga() {
    yield all([
        takeLatest(commonAction.EXAMPLE_REQUEST, getExample),
    ]);

}