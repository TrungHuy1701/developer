
const actions = {
    EXAMPLE_REQUEST: 'EXAMPLE_REQUEST',
    EXAMPLE_FAILURE: 'EXAMPLE_FAILURE',
    EXAMPLE_SUCCESS: 'EXAMPLE_SUCCESS',

    /*getExample*/
    getExample: (params) => ({
        type: actions.EXAMPLE_REQUEST,
        params
    }),

};

export default actions;