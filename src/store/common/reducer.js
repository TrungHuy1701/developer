import { commonAction } from '../action';

const initialState = {
    exampleResponse: null
};

export default (state = initialState, action) => {
    switch (action.type) {

        case commonAction.EXAMPLE_REQUEST:
            return {
                ...state,
                exampleResponse: {},
                isFetching: true,
                success: false,
                error: false
            };
        case commonAction.EXAMPLE_SUCCESS:
            return {
                ...state,
                exampleResponse: action.response,
                isFetching: false,
                success: true,
                error: false
            };
        case commonAction.EXAMPLE_FAILURE:
            return {
                ...state,
                exampleResponse: action.response,
                success: false,
                error: action.err
            };

        default:
            return state;
    }
}