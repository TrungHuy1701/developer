import { DatePicker, Input, Form, Dropdown } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { commonAction } from 'store/action';

const Elements = (props) => {

    const dispatch = useDispatch()

    const commonReducer = useSelector(state => state.commonReducer)
    const { exampleResponse } = commonReducer;

    useEffect(() => {
        dispatch(commonAction.getExample({}))
    }, [])

    // useEffect(() => {
    //     if (exampleResponse) console.log(exampleResponse)
    // }, [exampleResponse])

    return (
        <div className="container">
            <ul>
                <NavLink to="/">Login</NavLink>
            </ul>
            <h2>Element</h2>

            {
                exampleResponse &&
                <table>
                    <tbody>
                        <tr>
                            <td>Name:</td>
                            <td>{exampleResponse.name}</td>
                        </tr>
                        <tr>
                            <td>Id:</td>
                            <td>{exampleResponse.id}</td>
                        </tr>
                    </tbody>
                </table>
            }

        </div>
    );
}
export default Elements;
