import React, { Component } from 'react';

import Error from './Error.js'

const Page500 = () => {
    return (
        <Error code="500" description="Services error !" />
    )
}

export default Page500;
