import React, { Component } from 'react';

import Error from './Error.js'

const Page403 = () => {
    return (
        <Error code="403" description="Page not found !" />
    )
}

export default Page403;
 