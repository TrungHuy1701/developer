import React, { Component } from 'react';

import Error from './Error.js'

class PageErrorGlobal extends Component {
    render() {
        return (
            <Error hasDotsAnimation={true} code="" description="Vui lòng đợi hoặc thử lại sau!" />
        )
    }
}

export default PageErrorGlobal;
