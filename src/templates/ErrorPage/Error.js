import React, { Component } from 'react';

class Copyright extends Component {
    render() {
        return (
            <div className="card-footer bg-transparent">
                <div className="row">
                    <p className="text-muted text-center col-12 pb-1">Copyright © 2020 Minerva</p>
                </div>
            </div>
        )
    }
}

const Error = (props) => {
    return (
        <div className="vertical-center errorPage">
            <div className="card-header bg-transparent border-0">
                <i class="error-icon fas fa-exclamation mb-3"></i>
                <h1 className="error-code text-center mb-0" >{props.code ? props.code : 'Có lỗi xảy ra'}</h1>
                <i className="error-text text-center">{props.description ? props.description : 'Vui lòng đợi hoặc thử lại sau!'}</i>
                {props.hasDotsAnimation && <div class="dot-typing mt-4"></div>}
            </div>
        </div>
    )
}

export default Error;
