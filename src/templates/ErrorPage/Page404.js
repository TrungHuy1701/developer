import React, { Component } from 'react';

import Error from './Error.js'

const Page404 = () => {
    return (
        <Error code="404" description="Page not found !" />
    )
}

export default Page404;
