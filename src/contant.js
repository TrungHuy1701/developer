import { getLocalStore } from './functions/Utils';

// CONSTANT SOURCE====================================================================================================

/*services*/
export const CONTENT_TYPE = 'application/json; charset=utf-8';
export const CONTENT_MULITPART = 'multipart/form-data';
export const MNV_ENCODE = 0;
export const MNV_LANGUAGE = getLocalStore('language');
/*end services*/


export const LIMIT_PAGE_NOTIFICATION = 10 //1 lần show bao nhiêu item
export const LIMITED_AGE = 0 //giới hạn số tuổi 
export const LIMITED_LOGIN = 7 //giới hạn 7s
export const LIMITED_CAMERA = 12 //giới hạn 12 camera

export const TIME_REDIRECT_ERROR = 10000; //error sau 7s sẽ redirect đi chỗ khác
export const MOBILE_WIDTH = 767 //response cho màn hình
export const MAX_SIZE_IMAGE = 2500000; //limit size image
export const RATIO = 640 / 360 //ratio for camera

export const ERROR_SERVER = "error_server"; //check status error fail
export const IMAGE_URL = "../../"; //link image global
export const IMAGE_DEFAULT = "../../images/default_image.svg"; // image default khi ko có image
export const IMAGE_LOGO = "../../images/logo.png"; //logo
export const IMAGE_NO_AVATAR = "../../images/noavatar.png"; //for person no avatar


export const MAP = {
    TOKEN: 'pk.eyJ1IjoiYnJpYW5iYW5jcm9mdCIsImEiOiJsVGVnMXFzIn0.7ldhVh3Ppsgv4lCYs65UdA',
    STYLE: 'https://images.minerva.vn/Style/minerva'
};

export const STATUS_UPDATE = {
    INIT: 0,
    SUCCESS: 1,
    FAILURE: 2
}

export const NOTIFICATION_TYPE = {
    success: 'success',
    info: 'info',
    warning: 'warning',
    error: 'error',
}

export const FILE_CONTENT_TYPE = {
    DEFAULT: 'application/octet-stream',
    PDF: 'application/pdf',
    PNG: 'image/png',
    XPNG: 'image/x-png',
    JPEG: 'image/jpeg',
    JPG: 'image/jpg',
    GIF: 'image/gif',
    DOC: 'application/msword',
    DOCX: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    XLS: 'application/vnd.ms-excel',
    XLSX: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    DWG: 'image/vnd.dwg',
}

// CONSTANT DATA ====================================================================================================
