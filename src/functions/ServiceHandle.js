import { CONTENT_TYPE, ERROR_SERVER, MNV_ENCODE, MNV_LANGUAGE } from '../contant.js';
import { showNotification, translate } from './Utils.js';
var pako = require('pako');


function Encode(data) {
    var text = JSON.stringify(data);
    var buff = new TextEncoder("utf-8").encode(text);
    var compressed = pako.deflate(buff);
    return compressed;
}

function Decode(base64Data) {
    var decompressed = pako.inflate(base64Data);
    // var code = String.fromCharCode.apply(null, decompressed)
    var code = handleCodePoints(decompressed)
    var text = decodeURIComponent(escape(code))
    var data = JSON.parse(text);
    return data;
}

function handleCodePoints(array) {
    var CHUNK_SIZE = 0x8000; // arbitrary number here, not too small, not too big
    var index = 0;
    var length = array.length;
    var result = '';
    var slice;
    while (index < length) {
        slice = array.slice(index, Math.min(index + CHUNK_SIZE, length)); // `Math.min` is not really necessary here I think
        result += String.fromCharCode.apply(null, slice);
        index += CHUNK_SIZE;
    }
    return result;
}


/**
 * create url for request to server api
 * @params {*} url: response from getUrl()
 * @params {*} data: params from dispatch
 * @return {*} url full request to server api 
 */
function getUrl(url, data = {}) {

    // console.log('url:',url);

    var strUrl = url;
    var first = true;
    for (var key in data) {
        if (data[key] != null && data[key] != undefined && data[key] !== '') {
            strUrl = strUrl + (first ? '?' : '&') + (key + '=' + data[key]);
            first = false;
        }
    }
    // console.log('strUrl', strUrl);
    return strUrl;
}

/**
 * create header request
 * @params {*} token : token when login
 * @params {*} content_type : take from constant
 * @params {*} auth_type: Bearer or others
 * @return {*} header content
 */
function getHeader(token, content_type = CONTENT_TYPE, auth_type = 'Bearer') {
    let headers;
    if (token) {
        headers = {
            'Authorization': auth_type + ' ' + token,
            'MNV-encode': MNV_ENCODE,
            'MNV-LANGUAGE': MNV_LANGUAGE,
        }
    } else {
        headers = {
            'MNV-encode': MNV_ENCODE,
            'MNV-LANGUAGE': MNV_LANGUAGE,
        }
    }

    // console.log('headers request: ',headers);
    headers['Content-Type'] = content_type;

    return headers;
}


/**
 * Handle Response Buffer from handleRequest()
 * Noted: buffer dành cho xử lý video, stream and something for live....
 * 
 * @params {*} response : response from handleRequest()
 * @return {*} return data after Decode and excute buffer
 */
function handleResponseBuffer(response) {
    // fail 
    if (!response.ok) {
        var content = response.status + ' ' + response.statusText;
        if (response.status > 500 || response.status === 500) {
            showNotification({
                type: 'error',
                message: translate('error_server_message'),
                title: 'Error' + ' ' + response.status
            })
            return (Promise.reject(ERROR_SERVER));
        } else {
            showNotification({
                type: 'error',
                message: response.statusText,
                title: response.status
            })
        }
        return (Promise.reject(content));
    }

    // success 
    return response.arrayBuffer().then(buffer => {
        return Decode(buffer)
    });
}


/**
 * Handle Response Text from handleRequest()
 * @params {*} response : response from handleRequest()
 * @return {*} return data after Decode
 */
function handleResponseText(response) {

    // fail 
    if (!response.ok) {
        var content = response.status + ' ' + response.statusText;
        if (response.status >= 500) {
            showNotification({
                type: 'error',
                message: translate('error_server_message'),
                title: 'Error' + ' ' + response.status
            })
            return (Promise.reject(ERROR_SERVER));
        } else {
            showNotification({
                type: 'error',
                message: response.statusText,
                title: response.status,
                duration: 80000000000
            })
        }
        return (Promise.reject(content));
    }

    // success 
    return response.json().then(data => {
        // nếu token INVALID_TOKEN thì yêu cầu login lại
        if (data && data.success === false && data.code === "INVALID_TOKEN") {
            window.localStorage.clear();
            window.location.replace('/login')
        } else {
            return data
        }

    }).catch(e => console.log(e));
}

/**
 * handle body request (NORMAL)
 * @params {*} url : response from getUrl()
 * @params {*} options : params from dispatch
 * @return {*} send request and receive response 
 */
function handleRequest(url, options) {

    // console.log({url, options});

    options['url'] = url;
    if (options.hasOwnProperty("body")) {
        if (MNV_ENCODE === 1) {
            options['body'] = Encode(options['body']);
        }
        else {
            options['body'] = JSON.stringify(options['body'])
        }
    }

    if (MNV_ENCODE === 1) {
        return fetch(url, options).then(handleResponseBuffer);
    }
    return fetch(url, options).then(handleResponseText);
}



/**
 * update or create using (FROM-DATA)
 * @param {*} data: data FORM from params dispatch
 * @param {*} token : token when login
 * @param {*} url : response from getUrl()
 * @param {*} onProgress : identification processing progress
 * @return {*} response data 
 **/
function postWithFormData(data, token, url, onProgress) {
    return new Promise(function (resolve, reject) {
        const dataKeys = Object.keys(data);
        const formData = new FormData();
        for (let i in dataKeys) {
            let valueItem = data[dataKeys[i]]
            Array.isArray(valueItem)
                ? formData.append(dataKeys[i], JSON.stringify(valueItem))
                : formData.append(dataKeys[i], valueItem);
        }
        let request = new XMLHttpRequest();
        request.open("POST", url);
        onProgress && request.upload.addEventListener("progress", onProgress);
        request.setRequestHeader('MNV-ENCODE', MNV_ENCODE)
        request.setRequestHeader('Authorization', 'Bearer ' + token);
        request.send(formData);
        request.onreadystatechange = function () {
            if (request.readyState === XMLHttpRequest.DONE) {
                try {
                    let response = JSON.parse(request.responseText)
                    return resolve(response)
                }
                catch (e) {
                    return resolve(e.message);
                }
            }
        }
    });
}

export {
    getHeader,
    getUrl,
    handleRequest, //for resquest normal
    postWithFormData //for resquest form-data
}
