
module.exports = {
	NODE_ENV: 'none',
	MODE_ENV: {
		local: {
			api: 'https://pokeapi.co',
			cloudapi: 'https://cloudapi.minerva.vn',
			//dev
			show_log: true,
			hot: true,
			liveReload: true,
			host: 'localhost',
			port: 3004,
			portServer: 5004

		},
		dev: {
			api: 'https://cctvapp.minerva.vn',
			cloudapi: 'https://cloudapi.minerva.vn'
		},
		sta: {
			api: 'https://cctvapp.minerva.vn',
			cloudapi: 'https://cloudapi.minerva.vn'
		},
		prod: {
			api: 'https://cctvapp.minerva.vn',
			cloudapi: 'https://cloudapi.minerva.vn'
		},
	}
};
